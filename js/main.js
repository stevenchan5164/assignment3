var url2 = "https://www.googleapis.com/books/v1/volumes?q=isbn:9789863254928";
var read = "https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi/related";
var read1 = "https://itbilu.com/javascript/js/VkiXuUcC.html";
var read2 = "https://gist.github.com/nilcolor/816580";
var read3 = "http://johnzhang.io/options-request-in-express";
var read4 = "http://restify.com/docs/server-api/#opts";

window.onload = function() {    //Using to show the books table in system onload
    getalldata();               //Using get all data function
};

function getalldata(){          //Using to get and show all data of API
    var jsondata = null;        //Using to save jsondata
    var url = "https://stevenyuri177102277.herokuapp.com/getall";   //API path
    var xhr = new XMLHttpRequest(); //Using to do the request
    
    xhr.open("GET", url, true);     //Open the API use get 
    
    xhr.responseType = 'json';      //Set the response type

    xhr.onload = function () {      
        if (xhr.readyState === xhr.DONE) {
            if (xhr.status === 200) {
                jsondata = xhr.response;    //Save the APi return data to jsondata
                //jsondata = xhr.responseText;
                var code;                   //Using to read the table
                
                code="<tr><th>ISBN</th><th>Title</th><th>Author</th><th>Subtitle</th><th>Lend</th></tr>";
                
                for(var isbn in jsondata){  //Using for-loop to save the books data to the table
                    
                    var author = jsondata[isbn].authors;
                    var title = jsondata[isbn].title;
                    var lend = jsondata[isbn].lend;
                    var subtitlem = jsondata[isbn].subtitlem;
                    
                    code = code + "<tr><th>" + isbn +
                                  "</th><th>" + title + 
                                  "</th><th>" + author +
                                  "</th><th>" + subtitlem +
                                  "</th><th>" + lend + "</th></tr>";
                }
                
                document.getElementById("table1").innerHTML = code; //Write the table
                console.log("Show end");
            }
        }
    };

    xhr.send(null);
}

function getdata(){             //Using to search and get specific book data
    var isbn = document.getElementById("isbn").value;   //Get the book isbn and save to isbn
    
    if(isbn == ""){                                     //Checking the user input does null
        alert("Please input book isbn");                //Show error message
    }else{                                              //If the user has input isbn, do the search steps
    
    var url = "https://stevenyuri177102277.herokuapp.com/get?isbn=" + isbn; //API path and book isbn
    var xhr = new XMLHttpRequest(); //Using to do the request
    var jsondata;                   //Using to save jsondata
    
    xhr.open("GET", url, true);     //Open the API use get
    xhr.responseType = 'json';      //Set the response type

    xhr.onload = function () {
        if (xhr.readyState === xhr.DONE) {
            if (xhr.status === 200) {
                jsondata = xhr.response;    //Save the APi return data to jsondata
                //jsondata = xhr.responseText;
                //console.log(jsondata);
                
                if(jsondata == "ISBN invalid"){ //Check the APi return data
                    alert("ISBN invalid");      //If the isbn is invalid, show the error message
                }else{                          //Else, do the show steps

                jsondata = xhr.response;        //Save the APi return data to jsondata
                var code;                       //Using to read the table
                
                code="<tr><th>ISBN</th><th>Title</th><th>Author</th><th>Subtitle</th><th>Lend</th></tr>";
                
                
                    
                    var author = jsondata.authors;
                    var title = jsondata.title;
                    var lend = jsondata.lend;
                    var subtitlem = jsondata.subtitlem;
                    
                    code = code + "<tr><th>" + isbn +
                                  "</th><th>" + title + 
                                  "</th><th>" + author +
                                  "</th><th>" + subtitlem +
                                  "</th><th>" + lend + "</th></tr>";
                
                
                document.getElementById("table1").innerHTML = code; //Write the table
                
                }
            }
        }
    };

    xhr.send(null);
    
        
    }
}

function insertdata(){              //Using to insert the book
    var isbn = document.getElementById("isbn").value;       //Get the book isbn and save to isbn
    
    if(isbn == ""){                                         //Checking the user input does null
        alert("Please input book isbn");                    //Show error message
    }else{                                                  //If the user has input isbn, do the insert steps

    var url = "https://stevenyuri177102277.herokuapp.com/"; //API path
    var xhr = new XMLHttpRequest();                         //Using to do the request
    var jsondata;                                           //Using to save jsondata

    var data = "isbn=" + isbn;                              //Save the isbn
    
    xhr.open("POST", url, true);                            //Open the API use post
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  //Set the request header
    //xhr.setRequestHeader('isbn', isbn);
    xhr.responseType = 'text';                              //Set the response type
    
    xhr.onload = function () {
        var returndata = this.responseText;                 //Save the APi result to returndata
        alert(returndata);                                  //Show the result
        getalldata();                                       //Update table
        
        console.log(returndata);
    };

    xhr.send(data);                                         //Using to send the isbn to the API in body
    
    }
}

function deletedata(){                                      //Using to delete the book
    var isbn = document.getElementById("isbn").value;       //Get the book isbn and save to isbn
    
    if(isbn == ""){                                         //Checking the user input does null
        alert("Please input book isbn");                    //Show error message
    }else{                                                  //If the user has input isbn, do the delete steps
        
    var url = "https://stevenyuri177102277.herokuapp.com/"; //API path
    var xhr = new XMLHttpRequest();                         //Using to do the request

    var data = {"isbn": isbn};                              //Save the isbn
    var json = JSON.stringify(data);                        //Change the data to json format
    
    xhr.open("DELETE", url, true);                          //Open the API use delete
    //xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    //xhr.setRequestHeader('isbn', isbn);
    xhr.responseType = 'text';                              //Set the response type
    
    xhr.onload = function () {
        var returndata = this.responseText;                 //Save the APi result to returndata
        alert(returndata);                                  //Show the result
        getalldata();                                       //Update table
        
        console.log(returndata);
    };

    xhr.send(json);                                         //Using to send the json to the API in body
    
    }
}

function updatedata(){                                      //Using to update the book
    var isbn = document.getElementById("isbn").value;       //Get the book isbn and save to isbn
    
    if(isbn == ""){                                         //Checking the user input does null
        alert("Please input book isbn");                    //Show error message
    }else{                                                  //If the user has input isbn, do the delete steps
    
    var url = "https://stevenyuri177102277.herokuapp.com/"; //API path
    var xhr = new XMLHttpRequest();                         //Using to do the request

    var data = {"isbn": isbn};                              //Save the isbn
    var json = JSON.stringify(data);                        //Change the data to json format
    
    xhr.open("PUT", url, true);                             //Open the API use put
    //xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //xhr.setRequestHeader('X-PINGOTHER', 'pingpong');
    //xhr.setRequestHeader('Content-Type', 'application/xml');
    //xhr.setRequestHeader('Content-type','application/json');
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.responseType = 'text';                              //Set the response type
    
    xhr.onload = function () {
        var returndata = this.responseText;                 //Save the APi result to returndata
        alert(returndata);                                  //Show the result
        getalldata();                                       //Update table
        
        console.log(returndata);
    };

    xhr.send(json);                                         //Using to send the json to the API in body
    
    }
}

function listbyisbn(){                                      //List the books by book isbn
    var jsondata = null;                                    
    var url = "https://stevenyuri177102277.herokuapp.com/getall";
    var xhr = new XMLHttpRequest();
    
    xhr.open("GET", url, true);
    
    xhr.responseType = 'json';

    xhr.onload = function () {
        if (xhr.readyState === xhr.DONE) {
            if (xhr.status === 200) {
                jsondata = xhr.response;
                //jsondata = xhr.responseText;
                var code;
                
                code="<tr><th>ISBN</th><th>Title</th><th>Author</th><th>Subtitle</th><th>Lend</th></tr>";
                var arrarylist = {};
                var num = 0;
                for(var isbn in jsondata){                  //Using foreach to save the book data to the array
                    
                    var author = jsondata[isbn].authors;
                    var title = jsondata[isbn].title;
                    var lend = jsondata[isbn].lend;
                    var subtitlem = jsondata[isbn].subtitlem;
                    
                    arrarylist[num] = {"isbn": isbn, "author": author, "title": title, "lend": lend, "subtitle": subtitlem};    //Save the data to array
                    num = num +1;
                    
                }
                
                var array = arrarylist;
                
                for(var i=0; i<num; i++){                   //Using for-loop to re-write the array data by isbn
	                var save = array[i];                    //Using to save the book data
	                var save2 = i;                          //Using to save the book location
	                for(var x=i+1; x<num; x++){
	                    if(save.isbn < array[x].isbn){      //Checking the isbn and save the bigger isbn
	                        save = array[x];                //Change the book data
	                        save2 = x;                      //Change the book location
	                    }
	                }
	                
	                array[save2] = array[i]                 //Change the book data location
	                array[i] = save;                        
	                
	            }
	                
	            for(var a=0; a<num; a++){                   //Using for-loop to write the table
                code += "<tr><th>" + array[a].isbn + "</th><th>" 
                + array[a].title + "</th><th>"
                + array[a].author + "</th><th>"
                + array[a].subtitle + "</th><th>"
                + array[a].lend + "</th></tr>";
    }
                
                document.getElementById("table1").innerHTML = code; //Write the table
                console.log("Show end");
            }
        }
    };

    xhr.send(null);
}