//https://ctrlq.org/code/20020-query-book-by-isbn
var restify = require("restify");           //Create server
var request = require("request");           //Read thrid-party API return data
var url = require("url");                   //Get the data by url path
var firebase = require('firebase-admin');   //Link and manage firebase database
var jsondata;                               //Using to save the thrid-party API data
var data;                                   //Using to save the firebase data

var port = process.env.PORT || 8080;

firebase.initializeApp({                    //Firebase API key
  credential: firebase.credential.cert({
    projectId: 'probable-axon-136323',
    clientEmail: 'firebase-adminsdk-l5fi5@probable-axon-136323.iam.gserviceaccount.com',
    privateKey: '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCPts7ma2o6eVWq\nKCzwzDr1pEmYndlFC1idbagAIaorWDJfJDgQnU2LAMw64U1MhPbOGNDNMXiEdqxh\nTxkj4J0RJ/8HwvifUe5VxXP5bk5c9+XofZlugoQlGty72HaDaVnhb5EPNKilt4yv\nWA1Rj8EieWXDlC4yG1I4xeLkyRXeo2jSCn+cizhm13U7d1SgGx/KESdm4j6thbvY\n63MvnQ+QmtKbnRzFOGhnjbloeO6/0kEH8jPZPkshCe+tZSf5IlfV5S8wwO+SCsUS\n+XAcmQjgcyCg3rf09wj1+YXTlV9SF7/aI+FAp8jS99lpXQoa1PAgwPqnhEInKxjj\n2rwufu49AgMBAAECgf8fjJvfLOi+ukqtqb3NhW6fSz52dco8xHQWG2L5cTdQswv1\ncJerDhCRc2gvHnoM6/B1tJaIdVPMjXQXG9/PZuZHd4BSAK6T5M8HOITW1sbDog6j\nlO8Z7Sau/p0QOXT+MSKKJ8Cwld7WgnT5ovR8wBI/smbX7xY1HP2JeRrv65t7EUOG\n5QwyxyExihKiH+vvV6SWe1QTT6LiqrR9juYSZgl6ydYTm9TFTkroDm0te4qsl4/M\niWk3z1QnskPPHpkHaCn3ljK+7rfNn284VMR/2f1GVBs7cxfIPpgdVMnZR4j/Gyf5\nOgKQOWfaAG5TTvEBYKlRkPYWaJN05Cjv7qqPDAECgYEAwJviX8jupiSda1vMMAvW\ndJveKMzT89zofaesi2etiY+t7h6uSIe3crPKZ1gNeeQ8+0A19ceRwyXXxh3KMEe7\nE5Dla4p5oT+72utloCJea16BVSlVfM20YLqyh1IWlCtpkDmvSR9W4qQNnKS86LU9\nGmZHEPQUrX2A4tqP+AqfaQECgYEAvwNT+DpxK3FJOV85eB9dXg+RNl6oqKrK1oeB\ndcPZKaIP5RpWMiuR+xzDoEIHnDqh3Q1vq5lSlOU3iaZzwxaBuBbC4gVXafICN2Wv\nMyQMqMalWq180VQvogpr4Ih5Zpd16u/LN5s8/1jJ+a8JgXK3arMP3oYFnJDCu9QI\nPrbx6T0CgYEAuvFSCXfjsdxN8hq6F1QWBT4XpXAQtFuQSA2LWg76D08mGL0smXco\nZar9Y8rB4bHWQmKzPOdDoa5EPKVDThMBD1+OXQ+dOBW9BiF9lKxnCj1CuF6S+7xI\nO65Zgx/4jD2KixKCAC3rzbQ/Be+a7x6hvNLSXTEaNL1gcE6Ed9IPoQECgYAUvvnJ\nxh8whrdbQdpuD4oGg05UR2euGg65yjHnZoQZn352davS+yR5z5/3sfQ9paia57Cv\nRJHRliu4CZodID6qjd5Qyh+6ZAyVk7e5qDqvwhOHi7w4yBn8UYFx+6Cj4eNuxuid\ne4Degvg85CP2KU1+i0A3/PpzDM35tIfIt/oCSQKBgQCfUOjf4+02DBpgeKwcB6+o\nFBSuuVnsz/DNGOBeQE5CXpe/AdJ1ukkGey+ggUCDXt11qkXCGGNo3bEq3g6vFAzb\nIxYlpJNIzpMOFluPJXnzZLA1tOMc34gR5Zbz3N5BDi/0jooOLSuJKAadiUSXNVoj\nx2zJVmCMTU0laRoz7rnCxQ==\n-----END PRIVATE KEY-----\n'
  }),
  databaseURL: 'https://probable-axon-136323.firebaseio.com'
});

var server = restify.createServer();        //Create server
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.use(restify.plugins.authorizationParser());
var ref = firebase.database().ref().child('books'); //Firebase database table

function writeBookData(title, authors, subtitlem, isbn) { //Using to input the data to the database
    firebase.database().ref('books/' + isbn).set({
        title: title,                                     //Book title 
        authors: authors,                                 //Book author
        subtitlem: subtitlem,                             //Book subtitle
        lend: "false"                                     //lend
    });
}

function updateBookData(isbn) {                             //Using to update the book lend
    var checking = firebase.database().ref('books/' + isbn).child('lend');  //Get and save book lend path by database
        
    checking.once('value', function(snap){                  
        var lend = snap.val();                              //Get the lend value
        
        if(lend == "false"){                                //Check the lend
            firebase.database().ref('books/' + isbn).child('lend').set("true"); //Change lend
        }else{
            firebase.database().ref('books/' + isbn).child('lend').set("false");//Change lend
        }
    });
}

function deleteBookData(isbn) {
    firebase.database().ref('books/' + isbn).remove();      //Delete the book
}

server.listen(port,'0.0.0.0', function(){                             //Server REST
   console.log("incoming request being handled");
    
    server.opts("/*", function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
        res.send(200);
    });
    
    server.get('/getall', function(req, res, next) {        //Get all books data
        
         ref.once('value', function(snap){                   //Get the data by firebase books table
             data = snap.val();                              //Save the result
             res.send(data);                                 //Send data
             res.end();
         });
    });
    
    server.get('/get', function(req, res, next) {           //Get certain books data
        
        var input = url.parse(req.url, true).query;         //Get the book isbn by the url
        var data = firebase.database().ref('books/' + input.isbn); //Search and get the book data by database
            
        data.once('value', function(snap){                  
            var output = snap.val();                        //Save the result
                
            if (output != null){                            //Check the search result (if the result has data send to user, else send error message)
                res.send(output);                           //Send data
                res.end();
            }else{
                res.send("ISBN invalid");                   //Return error message  
                res.end();
            }
        });   
    });
    
    server.post('/', function(req, res){                    //Using the isbn to search the book data input to the database
        
        var isbn = req.body.isbn;                           //Get the isbn by body
        var url = "https://www.googleapis.com/books/v1/volumes?q=isbn:" + isbn; //Send the isbn to the third-party API path to get the book data
        
        request({                                           //Using the request to read the API return data to be a json format
            url: url,
            json: true
        }, function (error, response, body) {

        if (body.items) {                                   //Check the data is useful (if the json has items, do the input step eles the system will send a error message)
                jsondata = body;                            //Print the json response
                //var obj = JSON.parse(body);
                var title = body.items[0].volumeInfo.title; //Save and get the book title
                var authors = body.items[0].volumeInfo.authors[0]; //Save and get the book authors
                
                if(body.items[0].volumeInfo.subtitle){      //Check the book does have subtitle (If yes, save the subtitle else set to nil)
                    var subtitle = body.items[0].volumeInfo.subtitle;
                }else{
                    var subtitle = "Nil";
                }
                
                writeBookData(title,authors,subtitle,isbn); //Using function to input the data to database
                //console.log(body.items[0].volumeInfo);
                res.send("Book added");                     //Return message
                res.end();
            }else{
                jsondata = "Nil";
                res.send("ISBN invalid");                   //Return error message
                res.end();
            }
        });
    });
    
    server.put('/', function(req, res, next) {  //Using to update the book lend
        
        
        
        var isbn = req.body.isbn;               //Get the isbn by body
        //updateBookData(isbn);
        
        var checking = firebase.database().ref('books/' + isbn);    //Link to the firebase database to find this book using isbn
        
        checking.once('value', function(snap){                  
            var result = snap.val();                    //Get the result
                
            if (result != null){                        //Checking the result (if yes, do the update step, else return the error message)
                updateBookData(isbn);                   //Using function to do the update step
                res.send("Book updated");               //Return message
                res.end();
            }else{
                res.send("ISBN invalid");               //Return error message      
                res.end();
            }
        });
        
        
    });
    
    server.del('/', function(req, res, next) {  //Using to delete the book
        
        var isbn = req.body.isbn;               //Get the isbn by body
        //updateBookData(isbn);
        var checking = firebase.database().ref('books/' + isbn);    //Link to the firebase database to find this book using isbn
        
        checking.once('value', function(snap){                  
            var result = snap.val();                    //Get the result
                
            if (result != null){                        //Checking the result (if yes, do the delete step, else return the error message)   
                deleteBookData(isbn);                   //Using function to do the delete step
                res.send("Book deleted");               //Return message
                res.end();
            }else{
                res.send("ISBN invalid");               //Return error message      
                res.end();
            }
        });
    });
    
});
